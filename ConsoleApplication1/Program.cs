﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApplication1.ServiceReference1;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            var t = default(Task<Release[]>);
            var client = new ServiceReference1.VsIdeServiceClient();

            try
            {
                t = Search(client);
                t.Wait();

                client.Close();
            }
            catch (System.ServiceModel.CommunicationException)
            {
                client.Abort();
            }
            catch (TimeoutException)
            {
                client.Abort();
            }
            catch (AggregateException e)
            {
                client.Abort();
                if (e.InnerExceptions.Count == 1)
                {
                    throw e.InnerExceptions[0];
                }
                else
                {
                    throw;
                }
            }
            catch (Exception)
            {
                client.Abort();
                throw;
            }

            var microsoftItems = (from r in t.Result
                                  let p = r.Project
                                  let author = p.Metadata.First(_ => _.Key == "Author")
                                  where author.Value.StartsWith("Microsoft", StringComparison.CurrentCultureIgnoreCase)
                                  orderby p.Title
                                  select p).ToList();

            Console.WriteLine("Items: {0}", microsoftItems.Count);

            var csvResult = new StringBuilder();
            var htmlResult = new StringBuilder();

            foreach (var item in microsoftItems)
            {
                var downloadUrlMetaData = item.Metadata.FirstOrDefault(_ => _.Key == "DownloadUrl");
                var downloadUrl = downloadUrlMetaData.Value;
                var moreinfo = item.Metadata.First(_ => _.Key == "MoreInfoUrl").Value;
                var icon = item.Metadata.FirstOrDefault(_ => _.Key == "Icon").Value;

                Console.WriteLine(item.Title);

                htmlResult.AppendLine("<p>");
                if (!string.IsNullOrWhiteSpace(icon))
                {
                    htmlResult.AppendLine("<img style=\"border-left-width: 0px; border-right-width: 0px; background-image: none; border-bottom-width: 0px; float: left; padding-top: 0px; padding-left: 0px; display: inline; padding-right: 0px; border-top-width: 0px\" border=\"0\" align=\"left\" src=\"" + icon + "\" width=\"38\" height=\"38\"/>");
                }

                htmlResult.AppendLine("<strong>" + item.Title + "</strong>");
                htmlResult.AppendLine("</p>");
                htmlResult.AppendLine("<ul style=\"list-style-type: none; list-style-image: none;\">");
                htmlResult.AppendLine("<li style=\"background-image: none\">" + item.Description + "</li>");
                htmlResult.AppendLine("<li style=\"background-image: none\"><a href=\"" + moreinfo + "\" target=\"_blank\">More information</a></li>");
                htmlResult.AppendLine("<li style=\"background-image: none\"><a href=\"" + (downloadUrl ?? moreinfo) + "\" target=\"_blank\">Download</a></li>");
                htmlResult.AppendLine("</ul>");

                csvResult.AppendFormat("{0},", item.Title);
                csvResult.AppendLine(item.ModifiedDate.ToShortDateString());
            }

            var desktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            using (var file = File.CreateText(Path.Combine(desktop, "VisualStudioExtensions.csv")))
            {
                file.Write(csvResult.ToString());
            }

            using (var file = File.CreateText(Path.Combine(desktop, "VisualStudioExtensions.html")))
            {
                file.Write(htmlResult.ToString());
            }

            Console.WriteLine("Done writing");
            Console.ReadLine();
        }

        private async static Task<Release[]> Search(VsIdeServiceClient client)
        {
            var whereClause = "((Project.Metadata['SupportedVSEditions'] LIKE '%11.0,IntegratedShell;%') OR (Project.Metadata['SupportedVSEditions'] LIKE '%11.0,VSLS;%') OR (Project.Metadata['SupportedVSEditions'] LIKE '%11.0,Pro;%') OR (Project.Metadata['SupportedVSEditions'] LIKE '%11.0,Premium;%') OR (Project.Metadata['SupportedVSEditions'] LIKE '%11.0,Ultimate;%'))";
            var orderClause = "Project.Metadata['Ranking'] desc";
            var requestContext = new Dictionary<string, string>()
            {
                { "LCID", "1033" },
                {"SearchSource", "ExtensionManagerQuery"},
                {"OSVersion","6.2.9200.0"}
            };

            var result = await client.SearchReleases2Async("Microsoft", whereClause, orderClause, 0, 25, requestContext);
            return result.Releases;
        }
    }
}
